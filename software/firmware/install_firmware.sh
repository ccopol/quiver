#!/bin/bash

FW_DIR="$HOME/shared-j/devel/lulzbot/software/Marlin"
FW_VERSIONS=`(cd $FW_DIR; find 2.0.0.* -type d | tac); (cd archive; find 2.0.0.* -type d | tac)`
TOOLHEADS="DualExtruder HardenedSteel HardenedSteelPlus SingleExtruderAeroV2 SmallLayer"

echo
echo "Select version to install: "
echo
select fw in $FW_VERSIONS; do
  if [ ! -z "$fw" ]; then break; fi
done

echo
echo "Select toolhead to install: "
echo
select tool in $TOOLHEADS; do
  if [ ! -z "$tool" ]; then break; fi
done

FW_FILE=`ls $FW_DIR/$fw/Marlin_TAZPro_${tool}_${fw}_*.bin $FW_DIR/$fw/Marlin_TAZ7_${tool}_${fw}_*.bin archive/$fw/Marlin_Quiver_TAZ7_Quiver_${tool}_${fw}_*.bin | head -1`

echo
echo "Installing Marlin $fw for $tool"
echo "Binary file: $FW_FILE"
echo

while true; do
  ./bossac -i -U true -e -w -b -R "$FW_FILE"

  if [ $? -eq 0 ]; then
    echo
    echo "OK"
    break
  else
    echo
    echo
    echo
    echo "Did you push the PLEASE button? (it's a hole near the USB port)"
    echo
    read -p "Type ENTER to retry, Cntl-C to cancel"
  fi
done
