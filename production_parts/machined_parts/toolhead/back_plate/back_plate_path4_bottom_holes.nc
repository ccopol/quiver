(Exported by FreeCAD)
(Post Processor: linuxcnc_post)
(Output Time:2018-05-14 15:14:07.569763)
(begin preamble)
G17 G54 G40 G49 G80 G90
G21
(begin operation: T10: 2.35mm drill)
(machine: not set, mm/min)
(T10: 2.35mm drill)
M6 T10 G43 H10
M3 S800
M8
(finish operation: T10: 2.35mm drill)
(begin operation: Drilling)
(machine: not set, mm/min)
(Drilling)
(Begin Drilling)
G0 Z5.000
G90
G98
G83 X11.954 Y-3.750 Z-7.000 F120.000 Q1.000 R3.000
G80
G0 Z5.000
(finish operation: Drilling)
(begin postamble)
M05
G17 G54 G90 G80 G40
M2
