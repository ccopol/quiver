(Exported by FreeCAD)
(Post Processor: linuxcnc_post)
(Output Time:2018-05-15 08:01:44.350557)
(begin preamble)
G17 G54 G40 G49 G80 G90
G21
(begin operation: T10: 2.35mm drill)
(machine: not set, mm/min)
(T10: 2.35mm drill)
M6 T10 G43 H10
M3 S800
M8
(finish operation: T10: 2.35mm drill)
(begin operation: Drilling)
(machine: not set, mm/min)
(Drilling)
(Begin Drilling)
G0 Z5.000
G90
G98
G83 X18.493 Y-3.323 Z-12.000 F120.000 Q1.000 R3.000
G83 X45.460 Y-3.323 Z-12.000 F120.000 Q1.000 R3.000
G80
G0 Z5.000
(finish operation: Drilling)
(begin operation: T10: 2.35mm drill)
(machine: not set, mm/min)
(T10: 2.35mm drill)
M6 T10 G43 H10
M3 S800
(finish operation: T10: 2.35mm drill)
(begin operation: Drilling001)
(machine: not set, mm/min)
(Drilling001)
(Begin Drilling)
G0 Z5.000
G90
G98
G83 X25.367 Y-3.323 Z-5.000 F120.000 Q1.000 R3.000
G83 X37.828 Y-3.323 Z-5.000 F120.000 Q1.000 R3.000
G80
G0 Z5.000
(finish operation: Drilling001)
(begin postamble)
M05
G17 G54 G90 G80 G40
M2
